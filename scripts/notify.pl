#!/usr/bin/env perl

# Can't use dbus-send because the function org.freedesktop.Notifications.Notify
# uses a type that is not supported by dbus-send. From 'man dbus-send':
#
# "[...] dbus-send does not permit empty containers or nested containers (e.g.
#  arrays of variants)."

# TODO:
# - Make filtering configurable via setting

use Irssi;

sub pub_msg {
	my ($server, $msg, $nick, $address, $target) = @_;

	# Show only messages containing our own nick
	return() if (-1 == index($msg, $server->{nick}));

	# Filter messages in bitlbee channel
	return() if ($target eq "&bitlbee");

	system
		( "notify-send"
		, "-t", "10000"
		, "-c", "im.received"
		, "-i", "chat-message-new-symbolic"
		, "$target"
		, "$nick: $msg"
		)
}

sub priv_msg {
	my ($server, $msg, $nick, $address) = @_;

	system
		( "notify-send"
		, "-t", "10000"
		, "-c", "im.received"
		, "-i", "chat-message-new-symbolic"
		, "Private message"
		, "$nick: $msg"
		)
}

Irssi::signal_add_last('message public', \&pub_msg);
Irssi::signal_add_last('message private', \&priv_msg);
