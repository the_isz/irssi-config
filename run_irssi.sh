#!/bin/bash

which tmux &> /dev/null

if [ 0 -eq $? ]; then
	if [ "xx${TMUX}xx" != "xxxx" ]; then
		tmux split-window -d -h -l 30 "cat $HOME/.config/irssi/nicklistfifo"
	else
		echo "Not in a tmux session. Not showing nicklist."
	fi
else
	echo "tmux command not found. Not showing nicklist."
fi

irssi --home=$HOME/.config/irssi
